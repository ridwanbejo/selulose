from __future__ import unicode_literals

from django.db import models

class Institution(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	email = models.CharField(max_length=100, blank=True)
	phone = models.CharField(max_length=100, blank=True)
	address = models.CharField(max_length=200, blank=True)
	slug = models.SlugField()

	def __unicode__(self):
		return self.name

class Author(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(blank=True)
	institution = models.ForeignKey(Institution)
	email = models.CharField(max_length=100, blank=True)
	phone = models.CharField(max_length=100, blank=True)
	address = models.CharField(max_length=200, blank=True)
	slug = models.SlugField()
	
	def __unicode__(self):
		return self.name
