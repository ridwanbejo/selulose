from django.contrib import admin
from models import *

# Register your models here.
class AuthorAdmin(admin.ModelAdmin):
	list_display = ['name', 'email', 'phone', 'description', 'institution']
	list_filter = ('institution', )
	search_fields = ['name', 'description']
	list_per_page = 25
	prepopulated_fields = {"slug": ("name",)}
	
class InstitutionAdmin(admin.ModelAdmin):
	list_display = ['name', 'email', 'phone', 'description']
	search_fields = ['name', 'description']
	list_per_page = 25
	prepopulated_fields = {"slug": ("name",)}
	
admin.site.register(Author, AuthorAdmin)
admin.site.register(Institution, InstitutionAdmin)