from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from paper.models import *

import requests, json, logging

# Create your views here.
def search_journal(request):
	elk_result = []
	if request.GET.has_key('q') or request.POST.has_key('q'):
		q = ""
		if request.POST:
			q = request.POST['q']
		elif request.GET:
			q = request.GET['q']

		obj_url = "%s/%s/papers/_search" % (settings.ELASTICSEARCH_URL, settings.ELASTICSEARCH_INDEX)
	 	elk_data = {
	   		"query" : {
	   			"multi_match": {
	   				"query": q,
	   				"fields": ['content', 'abstract', 'tags', 'author', 'publisher'],
	   			},
	   			
	   		},
	   		"fields": [ 'title', 'author', 'publisher', 'tags', 'slug', 'category', 'id'],
		   		"highlight": {
		   			"fields": {
		   				"content":{}
		   			}
		   		}
	   		
	    }

		req = requests.get(obj_url, json=elk_data)
		resp = json.loads(req.text)
		
		if resp.has_key("error"):
		    logging.error(req.text)
		else:
			elk_result = resp['hits']['hits']
	return render(request, 'homepage/search.html', {'result':elk_result})

def index_journal(request):
	papers = Paper.objects.all().order_by('-created_at')
	paginator = Paginator(papers, 5)
	page = request.GET.get('page')
	try:
		papers = paginator.page(page)
	except PageNotAnInteger:
		papers = paginator.page(1)
	except EmptyPage:
		papers = paginator.page(paginator.num_pages)

	categories = Category.objects.all()
	return render(request, 'homepage/home.html', {'papers':papers, 'categories':categories})

def detail_journal(request, paper_id):
	temp_paper_id = paper_id.split('-')
	paper_id = temp_paper_id[len(temp_paper_id)-1]
	slug = temp_paper_id[:len(temp_paper_id)-1]
	slug = "-".join(slug)

	paper = Paper.objects.get(id=paper_id, slug=slug)
	return render(request, 'homepage/detail.html', {'paper':paper})

def browse_journal(request):
	
	if request.GET.has_key('category'):
		rows = Paper.objects.filter(category=Category.objects.get(slug=request.GET['category']))
		segment = 'paper'
	else:
		rows = Category.objects.all()
		segment = 'category'

	return render(request, 'homepage/browse.html', {"rows":rows, 'segment':segment})
	