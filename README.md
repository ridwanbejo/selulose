#Selulose - Simple Online Journal Repository

Dependencies:

- Django 1.9
- Elasticsearch
- SQLite3 / MySQL / PostgreSQL

Langkah instalasi:

- Pastikan Anda sudah mempunyai Python yang terinstall dan memasang django dengan menggunakan pip install
- klon repositori ini 
- Download Elasticsearch terbaru dari http://elastic.co
- Ekstrak  Elasticsearch dan simpan di folder favorit kamu
- Setting path ke /direktori_tempat_elasticsearch_berada/elasticearh-x.x.x/bin
- Jalankan Elasticsearch dengan mengeksekusi ./elasticsearch di Linux atau elasticsearch.exe di Windows
- Jalankan Django dengan: python manage.py runserver
- Voilla tinggal buka di browser aja
