"""selulose URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from homepage import views as homepage_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', homepage_views.index_journal, name='homepage-journal'),
    url(r'^search/', homepage_views.search_journal, name='homepage-search-journal'),
    url(r'^browse/', homepage_views.browse_journal, name='homepage-browse-journal'),
    url(r'^paper/(?P<paper_id>[^/]+)$', homepage_views.detail_journal, name='homepage-detail-journal'),
]
