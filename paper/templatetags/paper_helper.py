from django import template
from django.core import urlresolvers
from paper.models import Paper

register = template.Library()


@register.filter
def get_filename(url_name):
    filename = url_name.split('/')
    filename = filename[len(filename)-1]
    return filename

@register.filter
def get_paper_by_category(cat_id):
	paper_count = Paper.objects.all().filter(category=cat_id).count()
	return paper_count


@register.filter
def get_category_from_querystring(qs):
	category = qs['category']
	category = category.split('-')
	category = " ".join(category)
	return category