from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from author.models import Author
from tasks import dump_pdf2txt

import os

# Create your models here.
class Category(models.Model):
	name = models.CharField(max_length=60)
	slug = models.SlugField()
	description = models.TextField(blank=True)

	def __unicode__(self):
		return self.name

class Publisher(models.Model):
	name = models.CharField(max_length=100)
	slug = models.SlugField()
	description = models.TextField(blank=True)

	def __unicode__(self):
		return self.name

class Paper(models.Model):
	title = models.CharField(max_length=200)
	abstract = models.TextField(blank=True)
	text_dump = models.TextField(blank=True)
	tags = models.TextField(blank=True)
	slug = models.SlugField()
	publisher = models.ForeignKey(Publisher, null=True)
	category = models.ForeignKey(Category, null=True)
	author = models.ManyToManyField(Author, null=True)
	is_indexed = models.BooleanField(default=False)
	resource = models.FileField(upload_to=os.path.join(settings.MEDIA_ROOT,"papers"))
	created_at = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.title

@receiver(post_save, sender=Paper)
def dump_pdf2txt_event(sender, instance, *args, **kwargs):
    if kwargs.get("created"):
        dump_pdf2txt.delay(instance)
    return True