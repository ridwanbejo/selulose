from django.contrib import admin
from models import *
from tasks import *

# Register your models here.

def reindex_paper(modeladmin, request, queryset):
    for paper in queryset :
        # if not paper.is_indexed :
        update_index.delay(paper)
        
reindex_paper.short_description = "Reindex selected papers"

def create_index_paper(modeladmin, request, queryset):
    for paper in queryset :
        if not paper.is_indexed :
            create_index.delay(paper)
        
create_index_paper.short_description = "Create index for selected papers"

class PaperAdmin(admin.ModelAdmin):
	list_display = ['title', 'publisher', 'category', 'tags', 'is_indexed', 'resource']
	list_filter = ('publisher', 'category')
	search_fields = ['title', 'publisher', 'category', 'tags']
	list_per_page = 10
	prepopulated_fields = {"slug": ("title",)}
	actions = [reindex_paper, create_index_paper]

class CategoryAdmin(admin.ModelAdmin):
	list_display = ['name', 'description']
	search_fields = ['name', 'description']
	list_per_page = 25
	prepopulated_fields = {"slug": ("name",)}

class PublisherAdmin(admin.ModelAdmin):
	list_display = ['name', 'description']
	search_fields = ['name', 'description']
	list_per_page = 25
	prepopulated_fields = {"slug": ("name",)}
	
admin.site.register(Paper, PaperAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Publisher, PublisherAdmin)