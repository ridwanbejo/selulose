# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-05-13 15:59
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('paper', '0007_paper_is_index'),
    ]

    operations = [
        migrations.RenameField(
            model_name='paper',
            old_name='is_index',
            new_name='is_indexed',
        ),
    ]
