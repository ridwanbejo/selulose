from __future__ import absolute_import
from django.conf import settings

from shlex import split
from celery import shared_task
from subprocess import check_output
from datetime import datetime

from paper.models import *

import os, requests, json, logging


@shared_task
def dump_pdf2txt(instance):
    pdf_txt_dump = check_output('pdf2txt.py %s' % instance.resource.path, shell=True)
    pdf_txt_dump = pdf_txt_dump.replace('\n', '')
    pdf_txt_dump = pdf_txt_dump.replace('   ', '')
    instance.text_dump = pdf_txt_dump
    instance.save()

    return True

@shared_task
def create_index(instance):
    obj_url = "%s/%s/papers/%s" % (settings.ELASTICSEARCH_URL, settings.ELASTICSEARCH_INDEX, str(instance.id))
    
    elk_data = {
        'id': instance.id,
        'title': instance.title,
        'content': instance.text_dump,
        'publisher': instance.publisher.name,
        'slug': instance.slug,
        'category': instance.category.name,
        'author': [ author.name for author in instance.author.all() ] if instance.author else "",
    }

    req = requests.put(obj_url, json=elk_data)
    resp = json.loads(req.text)
    if resp.has_key("error"):
        logging.error(req.text)
    else:
        instance.is_indexed = True
        instance.save()

    return True

@shared_task
def update_index(instance):
    obj_url = "%s/%s/papers/%s/_update" % (settings.ELASTICSEARCH_URL, settings.ELASTICSEARCH_INDEX, str(instance.id))
    elk_data = { 
        "doc": {
            'id': instance.id,
            'title': instance.title,
            'content': instance.text_dump,
            'publisher': instance.publisher.name,
            'slug': instance.slug,
            'category': instance.category.name,
            'author': [ author.name for author in instance.author.all() ] if instance.author else "",
        }
    }

    req = requests.post(obj_url, json=elk_data)
    instance.is_indexed = True
    instance.save()

    return True